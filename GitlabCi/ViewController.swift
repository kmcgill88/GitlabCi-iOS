//
//  ViewController.swift
//  GitlabCi
//
//  Created by Kevin McGill on 5/23/17.
//  Copyright © 2017 McGill DevTech, LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    func add(a:Int, b:Int) -> Int {
        return a + b
    }
    
    func subtract(a:Int, b:Int) -> Int {
        return a - b
    }

}

