//
//  GitlabCiTests.swift
//  GitlabCiTests
//
//  Created by Kevin McGill on 5/23/17.
//  Copyright © 2017 McGill DevTech, LLC. All rights reserved.
//

import Quick
import Nimble

@testable import GitlabCi

class GitlabCiTests: QuickSpec {
    
    override func spec() {
        describe("ViewController", closure: {
            let viewController = ViewController()
            
            it("Adds two ints") {
                let result = viewController.add(a: 1, b: 1)
                expect(result) == 2
            }
            
            it("Adds subtracts two ints") {
                let result = viewController.subtract(a: 5, b: 1)
                expect(result) == 4
            }
        })
    }
}
